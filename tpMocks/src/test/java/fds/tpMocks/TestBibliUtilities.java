package fds.tpMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TestBibliUtilities {
	
	private NoticeBibliographique n1;
	private NoticeBibliographique n2;
	private NoticeBibliographique n3;
	private NoticeBibliographique n4;
	private NoticeBibliographique n5;
	private NoticeBibliographique n6;
	private NoticeBibliographique n7;
	private NoticeBibliographique n8;
	private NoticeBibliographique n9;
	private NoticeBibliographique n10;
	private NoticeBibliographique n11;
	private NoticeBibliographique n12;
	private NoticeBibliographique n13;
	private NoticeBibliographique n14;
	private ArrayList<NoticeBibliographique> listeNotices;
	private ArrayList<NoticeBibliographique> listeRetournee;
	private Bibliothèque b;
	
	@Mock
	Clock mockedClock;
	
	@Mock
	InterfaceBiblio mockedBddExtraiteDeInterface;
	
	@InjectMocks
	BibliUtilities bib = new BibliUtilities();
	
	@BeforeEach
	void setup() {
		n1 = new NoticeBibliographique("1", "Harry Potter à l'école des sorciers", "J.K. Rowling");
		n2 = new NoticeBibliographique("2", "Harry Potter et la chambre des secrets", "J.K. Rowling");
		n3 = new NoticeBibliographique("3", "Harry Potter et le prisonnier d'Azkaban", "J.K. Rowling");
		n4 = new NoticeBibliographique("4", "Harry Potter et la coupe de feu", "J.K. Rowling");
		n5 = new NoticeBibliographique("5", "Harry Potter et l'Ordre du phénix", "J.K. Rowling");
		n6 = new NoticeBibliographique("6", "Harry Potter et le Prince de sang-mélé", "J.K. Rowling");
		n7 = new NoticeBibliographique("7", "Harry Potter et les reliques de la Mort", "J.K. Rowling");
		n8 = new NoticeBibliographique("8", "Avant que le monde ne se ferme", "Alain Mascaro");	
		n9 = new NoticeBibliographique("9", "Premier sang", "Amélie Nothomb");
		n10 = new NoticeBibliographique("10", "Le kamasutra pour les nuls", "Benjamin Paul Jean Serva");
		n11 = new NoticeBibliographique("11", "51, pas qu'un département", "Benjamin Paul Jean Serva");
		n12 = new NoticeBibliographique("12", "Rouler sur le trottoir en fermant les yeux", "Benjamin Paul Jean Serva");
		n13 = new NoticeBibliographique("13", "Tour de bocal", "Tom Tisserand");
		n14 = new NoticeBibliographique("14", "Tour de bocal", "Tom Tisserand");
		
		listeNotices = new ArrayList<NoticeBibliographique>();
		listeRetournee = new ArrayList<NoticeBibliographique>();
		b = Bibliothèque.getInstance();
		b.addNotice(n2);
		b.addNotice(n14);
	}
	/*Test de chercherNoticesConnexes*/
	@Test
	public void testConnexe5TitresOuPlus() {
		listeNotices.add(n1);
		listeNotices.add(n2);
		listeNotices.add(n3);
		listeNotices.add(n4);
		listeNotices.add(n5);
		listeNotices.add(n6);
		listeNotices.add(n7);
		listeRetournee.add(n2);
		listeRetournee.add(n3);
		listeRetournee.add(n4);
		listeRetournee.add(n5);
		listeRetournee.add(n6);
		
		/*test valeurs successives pour le 120%*/
		when(mockedBddExtraiteDeInterface.noticesDuMemeAuteurQue(n1)).thenReturn(listeNotices, listeNotices= new ArrayList<NoticeBibliographique>());
		assertEquals(listeRetournee.size(),bib.chercherNoticesConnexes(n1).size());
		assertEquals(0,bib.chercherNoticesConnexes(n1).size());
	}
	
	@Test
	public void testConnexe1A4Titres() {
		listeNotices.add(n10);
		listeNotices.add(n11);
		listeNotices.add(n12);
		listeRetournee.add(n11);
		listeRetournee.add(n12);
		
		when(mockedBddExtraiteDeInterface.noticesDuMemeAuteurQue(n10)).thenReturn(listeNotices);
		assertEquals(listeRetournee.size(),bib.chercherNoticesConnexes(n10).size());
	}
	
	@Test
	public void testConnexe0Titre() {
		listeNotices.add(n8);
		when(mockedBddExtraiteDeInterface.noticesDuMemeAuteurQue(n8)).thenReturn(listeNotices);
		assertEquals(listeRetournee.size(),bib.chercherNoticesConnexes(n8).size());
	}
	
	@Test
	public void testConnexeTitreIdentique() {
		listeNotices.add(n13);
		listeNotices.add(n14);
		when(mockedBddExtraiteDeInterface.noticesDuMemeAuteurQue(n13)).thenReturn(listeNotices);
		assertEquals(listeRetournee.size(),bib.chercherNoticesConnexes(n13).size());
	}
	
	/*Test de ajoutNotice*/
	@Test
	public void testAjoutNoticeInexistante() throws IncorrectIsbnException {
		when(mockedBddExtraiteDeInterface.getNoticeFromIsbn("n20")).thenThrow(IncorrectIsbnException.class);
		assertThrows(AjoutImpossibleException.class, () -> {bib.ajoutNotice("n20");});
	}
	@Test
	public void testAjoutNoticeCorrecteNouvelle() throws IncorrectIsbnException, AjoutImpossibleException {
		when(mockedBddExtraiteDeInterface.getNoticeFromIsbn("9")).thenReturn(n9);
		assertEquals(NoticeStatus.newlyAdded,bib.ajoutNotice("9"));
	}
	
	@Test
	public void testAjoutNoticeCorrecteInchangee() throws IncorrectIsbnException, AjoutImpossibleException {
		when(mockedBddExtraiteDeInterface.getNoticeFromIsbn("2")).thenReturn(n2);
		assertEquals(NoticeStatus.nochange,bib.ajoutNotice("2"));
	}
	
	@Test
	public void testAjoutNoticeCorrecteMiseAJour() throws AjoutImpossibleException, IncorrectIsbnException {
		n14 = new NoticeBibliographique("14", "Tour de bocal 2.0", "Tom Tisserand");
		when(mockedBddExtraiteDeInterface.getNoticeFromIsbn("14")).thenReturn(n14);
		assertEquals(NoticeStatus.updated,bib.ajoutNotice("14"));
	}
	
/*test supplémentaire 120%*/	
	/*verifier que l'update a bien eu lieu et que seule la notice update figure dans la listeRetournee*/
	@Test
	public void testConnexeTitreNonIdentiqueApresUpdate() {
		listeNotices.add(n13);
		listeNotices.add(n14);
		n14 = new NoticeBibliographique("14", "Tour de bocal 2.0", "Tom Tisserand");
		listeNotices.add(n14);
		listeRetournee.add(n14);
		when(mockedBddExtraiteDeInterface.noticesDuMemeAuteurQue(n13)).thenReturn(listeNotices);
		assertEquals(listeRetournee.size(),bib.chercherNoticesConnexes(n13).size());
	}
	
	/*on test l'ordre en appliquant le 13 qui renvoie rien, puis le 14 qui renvoie le 13*/
	@Test
	public void testConnexeTitreNonIdentiqueApresUpdate2() {
		listeNotices.add(n13);
		listeNotices.add(n14);
		when(mockedBddExtraiteDeInterface.noticesDuMemeAuteurQue(n13)).thenReturn(listeNotices);
		assertEquals(listeRetournee.size(),bib.chercherNoticesConnexes(n13).size());
		n14 = new NoticeBibliographique("14", "Tour de bocal 2.0", "Tom Tisserand");
		listeNotices.add(n14);
		listeRetournee.add(n13);
		when(mockedBddExtraiteDeInterface.noticesDuMemeAuteurQue(n14)).thenReturn(listeNotices);
		assertEquals(listeRetournee.size(),bib.chercherNoticesConnexes(n14).size());
		InOrder ordre=inOrder(mockedBddExtraiteDeInterface);
		ordre.verify(mockedBddExtraiteDeInterface).noticesDuMemeAuteurQue(n13);
		ordre.verify(mockedBddExtraiteDeInterface).noticesDuMemeAuteurQue(n14);
	}
	
	/*verifier la réponse pour 2 fois la meme entrée*/
	@Test
	public void testAjoutNoticeCorrecteNouvellePuisInchangee() throws IncorrectIsbnException, AjoutImpossibleException {
		when(mockedBddExtraiteDeInterface.getNoticeFromIsbn("1")).thenReturn(n1,n1);
		assertEquals(NoticeStatus.newlyAdded,bib.ajoutNotice("1"));
		assertEquals(NoticeStatus.nochange,bib.ajoutNotice("1"));
		verify(mockedBddExtraiteDeInterface, times(2)).getNoticeFromIsbn("1");
		
	}
	
	/*Test de prévoirInventaire*/
	@Test
	public void testPrévoirInventaire() {
		
		LocalDate uneDate = LocalDate.of(2021, 05, 01);
		Clock fixedClock = Clock.fixed(uneDate.atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault());
		doReturn(fixedClock.instant()).when(mockedClock).instant();
		doReturn(fixedClock.getZone()).when(mockedClock).getZone();
		assertEquals(false, bib.prévoirInventaire());
		
		/*les deux tests sont dans un seul test parce qu'on manipule une même instance de bibliothèque*/
		LocalDate autreDate = LocalDate.of(2025, 12, 31);
		fixedClock = Clock.fixed(autreDate.atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault());
		doReturn(fixedClock.instant()).when(mockedClock).instant();
		doReturn(fixedClock.getZone()).when(mockedClock).getZone();
		assertEquals(true, bib.prévoirInventaire());
	}
}

