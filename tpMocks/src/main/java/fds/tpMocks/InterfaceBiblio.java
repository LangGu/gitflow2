package fds.tpMocks;

import java.util.ArrayList;

public interface InterfaceBiblio {

	NoticeBibliographique getNoticeFromIsbn(String isbn) throws IncorrectIsbnException;

	ArrayList<NoticeBibliographique> noticesDuMemeAuteurQue(NoticeBibliographique ref);

	ArrayList<NoticeBibliographique> autresEditions(NoticeBibliographique ref);

}