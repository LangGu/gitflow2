package fds.tpMocks;

import java.time.Clock;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class BibliUtilities {
	Clock horloge;
	InterfaceBiblio BddExtraiteDeInterface = new GlobalBibliographyAccess();
	Bibliothèque Biblio = Bibliothèque.getInstance();
	
	public ArrayList<NoticeBibliographique> chercherNoticesConnexes(NoticeBibliographique ref){
		/*creation liste à renvoyer qui accueillera les notices de meme auteur que la ref, taille limite 5*/
		ArrayList<NoticeBibliographique> connexes = new ArrayList<NoticeBibliographique>(5);
		/*récuperation du titre de la ref envoyé en paramètre*/
		String title = ref.getTitre();
		/*creation liste de toutes les notices du meme auteur que la ref*/
		ArrayList<NoticeBibliographique> listeOeuvreMemeAuteur = BddExtraiteDeInterface.noticesDuMemeAuteurQue(ref);
		/*compter le nombre de notice ajouté au resultat final : connexes*/
		int i = 0;
		/*compter le nombre de notice parcouru dans listeOeuvreMemeAuteur*/
		int cpt = 0;
		/*taille de la listeOeuvreMemeAteur*/
		int longeurBddExtraite= listeOeuvreMemeAuteur.size();
		
		while (i<5 && cpt<longeurBddExtraite) { /* on utilise pas contains car (string)*/
			/*recuperation du titre de la notice dans listeOeuvre*/
			String ajoutPotentiel = listeOeuvreMemeAuteur.get(cpt).getTitre();
			/*si le titre n'est pas different on incrémente cpt et on recommence while*/
			if (title != ajoutPotentiel) {
				/*si le boolean reste true on pourra ajouter la notice, sinon c'est qu'elle est deja dans connexes*/
				boolean pasContenuDans = true;
				/*recherche dans connexe d'un doublon*/
				for (int j = 0; j< i;j++) {
					 if (connexes.get(j).getTitre()==ajoutPotentiel)
						 pasContenuDans = false;
				}
				if (pasContenuDans) {
					connexes.add(listeOeuvreMemeAuteur.get(i));
					/*incrementation seulement si ajout dans connexes*/
					i++;
				}		
			}cpt++;
		}
		return connexes;
	}

    public NoticeStatus ajoutNotice(String isbn) throws AjoutImpossibleException {
        NoticeBibliographique notice;
        /* essaye d'obtenir la notice correspondante à l'ISBN, s'il n'y en a pas, l'exception de type est levée IncorrectIsbnException*/
        try {
            notice = BddExtraiteDeInterface.getNoticeFromIsbn(isbn);
        } catch (IncorrectIsbnException ex) {
            throw new AjoutImpossibleException();
        }
        /* retourne la biblio avec la notice ajoutée*/
        return Bibliothèque.getInstance().addNotice(notice);
        
    } 
    
    
    /*nous n'avons pas implementé le lancement effectif de l'inventaire si effectiement le bool est True, car c'est un 
    /*effect de bord qui ne laisse pas le choix à l'utilisateur (sauf si input, puis tester un input...), il lancera lui meme 
    /* inventaire() car c'est une methode public de la classe Bibliothèque, la main à l'utilisateur */
	
    public boolean prévoirInventaire() {
        /*obtention de la date actuelle*/
        //LocalDate horloge =  LocalDate.now(Clock.systemDefaultZone());
    	LocalDate jour =  LocalDate.now(horloge);
        /*obtention de la date du dernier inventaire*/
        LocalDate dateDernierInventaire = Biblio.getLastInventaire();
        /*ecartJours est l'écart entre les deux dates en jour, s'il est supérieur à 365 alors il faut prévoir un inventaire*/
        final long ecartJours = ChronoUnit.DAYS.between(dateDernierInventaire, jour);
        return (ecartJours >=365);
    }
	
}
